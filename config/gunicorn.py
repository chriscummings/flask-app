#!/usr/bin/env python

from multiprocessing import cpu_count
from os import environ
import os

dir = os.path.dirname(os.path.realpath(__file__))

# http://docs.gunicorn.org/en/latest/configure.html

def max_workers():    
    return cpu_count()

#bind = '0.0.0.0:' + environ.get('PORT', '8000')
#bind = 'unix:/tmp/gunicorn_flask.sock'

max_requests = 1000
workers = max_workers() * 2 + 1
timeout = 30
preload = True

backlog = 2048
#worker_class ="sync"
#worker_class =  "gevent"
debug = True
#daemon = True
pidfile ="/tmp/gunicorn.pid"
logfile = os.path.join(dir, 'log', 'gunicorn.log') # shared too
accesslog = os.path.join(dir, 'log', 'access.log')
errorlog = os.path.join(dir, 'log', 'errors.log')
#loglevel = '' # debug, info, warning, error, critical
