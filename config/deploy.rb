set :application, "flask-app"
set :repository,  "git@github.com:chriscummings/flask-app.git"

role :web, "108.171.184.222"            
role :app, "108.171.184.222"                         
role :db,  "108.171.184.222", :primary => true # This is where Rails migrations will run
role :db,  "108.171.184.222"

set :scm, :git
set :deploy_via, :remote_cache
set :user, "app_user"
set :deploy_to, "/var/rails/#{application}"
set :runner, "app_user"
set :use_sudo, false

namespace :deploy do
  #task :start do ; end
  #task :stop do ; end
  #task :restart do ; end
  #task :update_code do ; end #override this task to prevent capistrano to upload on servers
  #task :symlink do ; end #don't create the current symlink to the last release
  #task :migrate do ; end # don't auto migrate
  
  deploy.task :finalize_update do
    run "chmod -R g+w #{latest_release}" if fetch(:group_writable, true)
  
    # We're not using Rails asset directories
    #stamp = Time.now.utc.strftime("%Y%m%d%H%M.%S")
    #asset_paths = %w(img js css).map { |p| "#{latest_release}/static/#{p}" }.join(" ")
    #run "find #{asset_paths} -exec touch -t #{stamp} {} ';'; true", :env => { "TZ" => "UTC" } 
  end
end

# task :gunicorn_start do
#   run "cd #{current_path} ; virtenv/bin/gunicorn -c config/gunicorn.py app:app"
# end
# 
# after 'deploy:update_code', 'bundle'
# 
# 
# 
task :bundle do
  # Install virtualenv
  run "cd #{current_path} ; virtualenv virtenv"
  # Install dependencies
  run "cd #{current_path} ; virtenv/bin/pip install -r config/requirements.txt"
end