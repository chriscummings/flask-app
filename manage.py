#!/usr/bin/env python
import os

from flask.ext.script import Manager, Shell, Server

from app import app

manager = Manager(app)
manager.add_command("server", Server())
manager.add_command("shell", Shell())

@manager.command
def bundle():
	os.system('virtenv/bin/pip install -r config/requirements.txt')

@manager.command
def bundle():
	"""
	Installs dependencies from requirements.txt using pip in much the same 
	way as running `bundle` would in a rails app.
	"""
	os.system('virtenv/bin/pip install -r config/requirements.txt')

@manager.command
def gunicorn_start():
	os.system('virtenv/bin/gunicorn -c config/gunicorn.py app:app')

if __name__ == "__main__":
	manager.run()